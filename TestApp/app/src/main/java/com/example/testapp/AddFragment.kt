package com.example.testapp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.fragment_add.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddFragment : Fragment() {

    lateinit var dbHelper: DBHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Log.d("AddFragment", "fragment created")



        confirmAdd.setOnClickListener {
            var taskPriority = when (priority.text.toString()) {
                "low" -> TaskPriority.low
                "medium" -> TaskPriority.medium
                "high" -> TaskPriority.high
                "critical" -> TaskPriority.critical
                else -> TaskPriority.critical
            }

            dbHelper.addTask(
                TaskItem(
                    0,
                    name.text.toString(),
                    taskPriority,
                    date.text.toString(), false
                )
            )

            //Toast.makeText(activity, "Added", Toast.LENGTH_SHORT)
            activity?.supportFragmentManager?.popBackStack()

        }


        dbHelper = DBHelper(activity!!)

        //dbHelper.addTask(TaskItem(10, "Pranie 2", TaskPriority.low, Date(2018, 12, 12)))
        Log.d("AddFragm", "Added Task")
    }


}
