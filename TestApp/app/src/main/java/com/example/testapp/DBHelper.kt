package com.example.testapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

class DBHelper(context: Context) :
    SQLiteOpenHelper(context, DATA_BASE_NAME, null, DATA_BASE_VERSION) {

    companion object {
        internal const val DATA_BASE_NAME = "TASKS_DATABASE.db"
        internal const val DATA_BASE_VERSION = 10

        internal const val TABLE_NAME = "tasks"
        internal const val COLUMN_ID = "id"
        internal const val COLUMN_NAME = "name"
        internal const val COLUMN_PRIORITY = "priority"
        internal const val COLUMN_DATE = "date"
        internal const val COLUMN_DONE = "done"
    }

    private val createTasksTable = (
            "CREATE TABLE $TABLE_NAME (" +
                    "$COLUMN_ID INTEGER PRIMARY KEY, " +
                    "$COLUMN_NAME TEXT NOT NULL, " +
                    "$COLUMN_PRIORITY TEXT NOT NULL, " +
                    "$COLUMN_DATE STRING NOT NULL, " +
                    "$COLUMN_DONE INTEGER NOT NULL" +
                    ");"
            )

    private val dropTasksTable = (
            "DROP TABLE IF EXISTS $TABLE_NAME;"
            )

    private val getTasks = (
            "SELECT * FROM $TABLE_NAME order by $COLUMN_PRIORITY;"
            )

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(createTasksTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(dropTasksTable)
        db?.execSQL(createTasksTable)
    }

    val allTasks: ArrayList<TaskItem>
        get() {
            val db = this.readableDatabase
            val cursor = db.rawQuery(getTasks, null)

            var taskList = ArrayList<TaskItem>()

            if (cursor.moveToFirst()) {
                do {
                    val id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID))
                    val name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME))
                    val priority = cursor.getString(cursor.getColumnIndex(COLUMN_PRIORITY))
                    val date = cursor.getString(cursor.getColumnIndex(COLUMN_DATE))
                    val done = when (cursor.getInt(cursor.getColumnIndex(COLUMN_DONE))) {
                        1 -> true
                        0 -> false
                        else -> true
                    }


                    var taskPriority = when (priority) {
                        "low" -> TaskPriority.low
                        "medium" -> TaskPriority.medium
                        "high" -> TaskPriority.high
                        "critical" -> TaskPriority.critical
                        else -> TaskPriority.critical
                    }

                    taskList.add(
                        TaskItem(
                            id,
                            name,
                            taskPriority,
                            date,
                            done
                        )
                    )
                } while (cursor.moveToNext())
            }

            cursor.close()
            db.close()

            return taskList
        }

    fun addTask(task: TaskItem) {
        val newRecord = ContentValues()

        newRecord.put(COLUMN_NAME, task.taskName)
        newRecord.put(COLUMN_PRIORITY, task.priority.toString())
        newRecord.put(
            COLUMN_DATE,
            task.taskDate
        )
        var done = when (task.done) {
            true -> 1
            false -> 0
        }

        newRecord.put(COLUMN_DONE, done)

        val db = this.writableDatabase

        db.insert(TABLE_NAME, null, newRecord)
        db.close()
    }

    fun editTask(task: TaskItem, id: Long) {
        val updateRecord = ContentValues()

        updateRecord.put(COLUMN_NAME, task.taskName)
        updateRecord.put(COLUMN_PRIORITY, task.priority.toString())
        updateRecord.put(
            COLUMN_DATE,
            task.taskDate
        )

        var done = when (task.done) {
            true -> 1
            false -> 0
        }

        updateRecord.put(COLUMN_DONE, done)

        val db = this.writableDatabase

        db.update(TABLE_NAME, updateRecord, "$COLUMN_ID=$id", null)
        db.close()
    }

    fun deleteTask(id: Long) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "$COLUMN_ID=$id", null)
        db.close()
    }
}