package com.example.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_edit.date
import kotlinx.android.synthetic.main.activity_edit.name
import kotlinx.android.synthetic.main.activity_edit.priority
import kotlinx.android.synthetic.main.activity_edit.view.*
import kotlinx.android.synthetic.main.fragment_add.*
import java.util.*

class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        var dbHelper = DBHelper(this)

        var id = getIntent().extras?.get("id")

        var taskItem = dbHelper.allTasks.find { it.id == id }!!
        name.setText(taskItem.taskName)
        date.setText(
            taskItem.taskDate
        )
        priority.setText(taskItem.priority.toString())

        delete.setOnClickListener {
            dbHelper.deleteTask(id as Long)
            this.finish()
        }

        confirm.setOnClickListener {

            var taskPriority = when (priority.text.toString()) {
                "low" -> TaskPriority.low
                "medium" -> TaskPriority.medium
                "high" -> TaskPriority.high
                "critical" -> TaskPriority.critical
                else -> TaskPriority.critical
            }

            var done = false
            if (checkBox.isChecked) {
                done = true
            }

            var task = TaskItem(
                0,
                name.text.toString(),
                taskPriority,
                date.text.toString(),
                done
            )

            dbHelper.editTask(
                task,
                id as Long
            )

            this.finish()
        }
    }


}
