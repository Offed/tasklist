package com.example.testapp


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    lateinit var dbHelper: DBHelper
    lateinit var tasks: ArrayList<TaskItem>
    lateinit var adapter: TaskListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("ListFragment", "created")

        dbHelper = DBHelper(activity!!)

        //dbHelper.addTask(TaskItem(0, "Pranie", TaskPriority.critical, Date(2018, 11, 12)))

        var tasks = dbHelper.allTasks
        tasks = ArrayList(tasks.filter { it.done != true })
        tasks.sortByDescending { it.priority }



        adapter = TaskListAdapter {
            var intent = Intent(activity, EditActivity::class.java)
            intent.putExtra("id", it)
            startActivity(intent)
        }


        /*buttonAdd.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.CLmainActivity, AddFragment())?.addToBackStack("X")?.commit()
        }*/

        RVtaskList.adapter = adapter
        RVtaskList.layoutManager = LinearLayoutManager(activity)

        adapter.setTasks(tasks)

    }

    override fun onResume() {
        super.onResume()
        tasks = dbHelper.allTasks
        tasks.sortByDescending { it.priority }
        tasks = ArrayList(tasks.filter { it.done != true })
        adapter.setTasks(tasks)
    }
}
