package com.example.testapp

import android.provider.BaseColumns

class DBContract {

    /* Inner class that defines the table contents */
    class TaskEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "tasks"
            val COLUMN_TASK_ID = "task_id"
            val COLUMN_NAME = "task_name"
            val COLUMN_PRIORITY = "task_priority"
            val COLUMN_DATE = "task_date"
        }
    }
}