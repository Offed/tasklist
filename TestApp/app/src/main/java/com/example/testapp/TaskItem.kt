package com.example.testapp

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

data class TaskItem(public val id : Long, val taskName: String, val priority: TaskPriority, val taskDate: String, val done : Boolean)