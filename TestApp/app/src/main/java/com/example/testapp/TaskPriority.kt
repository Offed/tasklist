package com.example.testapp

enum class TaskPriority(name: String) {
    low("Niski"),
    medium("Średni"),
    high("Wysoki"),
    critical("Krytyczny")
}