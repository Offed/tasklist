package com.example.testapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.task_item_layout.view.*

class TaskListAdapter(val onItemClick: (Long) -> Unit) :
    RecyclerView.Adapter<TaskListAdapter.TaskListViewHolder>() {


    private var tasks = ArrayList<TaskItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.task_item_layout, parent, false)

        return TaskListViewHolder(itemView)
    }

    override fun getItemCount(): Int = tasks.size

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        holder.taskName.text = tasks[position].taskName
        holder.taskDate.text = tasks[position].taskDate
        holder.taskPriority.text = tasks[position].priority.toString()
    }

    fun setTasks(tasksList: ArrayList<TaskItem>) {
        tasks = tasksList
        notifyDataSetChanged()
    }

    fun addTask(task: TaskItem) {
        tasks.add(task)
        notifyItemInserted(tasks.size - 1)
    }

    inner class TaskListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val taskName = view.name
        val taskPriority = view.priority
        val taskDate = view.date

        init {
            view.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemClick(tasks[adapterPosition].id)
                }
            }
        }
    }
}